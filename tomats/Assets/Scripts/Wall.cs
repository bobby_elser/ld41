﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Wall : MonoBehaviour
{
    private List<Rigidbody2D> rbs;

    private void Start()
    {
        rbs = new List<Rigidbody2D>();
    }

    private void LateUpdate()
    {
        foreach (Rigidbody2D rb in rbs)
        {
            // Only negate "downward" component of velocity, ignore "upward"
            if (rb.velocity.magnitude > 0 && Vector3.Angle(rb.velocity, -transform.up) < 90)
            {
                rb.velocity = Vector3.Project(rb.velocity, transform.right);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Rigidbody2D rb = collision.gameObject.GetComponent<Rigidbody2D>();
        if (rb != null && !rbs.Contains(rb))
        {
            rbs.Add(rb);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Rigidbody2D rb = collision.gameObject.GetComponent<Rigidbody2D>();
        if (rb != null && rbs.Contains(rb))
        {
            rbs.Remove(rb);
        }
    }
}
