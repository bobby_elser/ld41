﻿using UnityEngine;
using UnityEditor;

public class Minimap : MonoBehaviour
{
    public MapVariable CurrentMapVariable;
    public SpriteRenderer MarkerPrefab;
    public Camera MapCamera;
    public Vector2Variable PlayerMapPositionVariable;

    private Transform playerMarker;
    private Transform islandMarker;
    private Rect worldViewRect;

    private void Update()
    {
        MapData map = CurrentMapVariable.Value;

        Vector3 bottomLeftCorner = MapCamera.ViewportToWorldPoint(new Vector3(0, 0, MapCamera.nearClipPlane));
        Vector3 topRightCorner = MapCamera.ViewportToWorldPoint(new Vector3(1, 1, MapCamera.nearClipPlane));
        Rect newViewRect = new Rect(bottomLeftCorner, topRightCorner - bottomLeftCorner);
        if (newViewRect != worldViewRect)
        {
            if (playerMarker == null)
            {
                SpriteRenderer marker = Instantiate(MarkerPrefab, transform);
                marker.color = Color.white;
                playerMarker = marker.transform;
            }
            if (islandMarker == null)
            {
                SpriteRenderer marker = Instantiate(MarkerPrefab, transform);
                marker.color = Color.green;
                islandMarker = marker.transform;
            }
            worldViewRect = newViewRect;
        }
        islandMarker.position = worldViewRect.center;
        playerMarker.localPosition = PlayerMapPositionVariable.Value * (newViewRect.width / map.MapWidth);
    }
}