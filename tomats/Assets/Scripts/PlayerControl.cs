﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public float forwardSpeed = 10f;
    public float turnSpeed = 300f;

    private Vector2 forwardDirection = Vector2.up;
    private Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    public void Update()
    {
        float rotation = -Input.GetAxis("Horizontal") * turnSpeed;
        float travel = Input.GetAxis("Vertical") * forwardSpeed;
        rb.angularVelocity = rotation;
        forwardDirection = transform.up;
        rb.velocity = (Vector3)forwardDirection * travel;
    }
}
