﻿using System;

[Serializable]
public class SpiritData
{
    public CreatureSpawnAreaData SpawnArea;
    public SpiritKeyData Key;
}