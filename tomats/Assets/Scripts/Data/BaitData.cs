﻿using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "LD41/Bait Data")]
public class BaitData : HoldableItem
{
    public Color PickupColor;
}