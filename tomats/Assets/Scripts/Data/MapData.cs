﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;

[CreateAssetMenu(menuName="LD41/Map Data")]
public class MapData : ScriptableObject
{
    public float MapWidth;
    public float MapHeight;
    public List<CreatureSpawnAreaData> CreatureSpawnAreas;
    public List<SpiritData> Spirits;
    public List<BaitData> StarterBaits;
}