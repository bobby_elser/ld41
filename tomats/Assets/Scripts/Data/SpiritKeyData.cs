﻿using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "LD41/Spirit Key")]
public class SpiritKeyData : ScriptableObject
{
    public GameEvent KeyAddedEvent;
}