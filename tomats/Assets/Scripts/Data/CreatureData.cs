﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CreateAssetMenu(menuName ="LD41/Creature Data")]
public class CreatureData : ScriptableObject
{
    public string ID;
    public float ShadowSize = 1f;
    public BaitData BaitDropped;
    public List<BaitData> FavoredBait;
}