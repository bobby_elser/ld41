﻿using System;
using UnityEngine;

[Serializable]
public class CreatureSpawnAreaData
{
    public Vector2 Position;
    public float Radius;
    public int TargetPopulation;
    public float SpawnTime;
    public CreatureData CreatureToSpawn;
}