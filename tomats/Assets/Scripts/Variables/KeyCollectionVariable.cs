﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "LD41/SpiritKey Collection Variable")]
public class KeyCollectionVariable : ScriptableObject
{
    public List<SpiritKeyData> Value;

    private void OnEnable()
    {
        Value = new List<SpiritKeyData>();
    }
}
