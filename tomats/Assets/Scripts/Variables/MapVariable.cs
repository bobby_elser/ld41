﻿using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "LD41/Map Variable")]
public class MapVariable : ScriptableObject
{
    [SerializeField]
    private MapData InitialValue;
    public MapData Value;

    private void OnEnable()
    {
        Value = InitialValue;
    }
}