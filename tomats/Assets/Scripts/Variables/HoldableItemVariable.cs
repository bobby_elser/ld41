﻿using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "LD41/HoldableItem Variable")]
public class HoldableItemVariable : ScriptableObject
{
    [SerializeField]
    private HoldableItem InitialValue;
    public HoldableItem Value;

    private void OnEnable()
    {
        Value = InitialValue;
    }
}