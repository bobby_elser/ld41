﻿using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "LD41/Vector2 Variable")]
public class Vector2Variable : ScriptableObject
{
    [SerializeField]
    private Vector2 InitialValue;
    public Vector2 Value;

    private void OnEnable()
    {
        Value = InitialValue;
    }
}