﻿using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "LD41/Int Variable")]
public class IntVariable : ScriptableObject
{
    [SerializeField]
    private int InitialValue;
    public int Value;

    private void OnEnable()
    {
        Value = InitialValue;
    }
}