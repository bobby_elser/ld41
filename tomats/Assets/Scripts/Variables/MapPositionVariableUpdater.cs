﻿using UnityEngine;
using System.Collections;

public class MapPositionVariableUpdater : MonoBehaviour
{
    public Vector2Variable MapPositionVariable;

    void Update()
    {
        MapPositionVariable.Value = transform.position;
    }
}
