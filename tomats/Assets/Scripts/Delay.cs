﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class Delay : MonoBehaviour
{
    public float DelaySeconds = 3f;
    public UnityEvent DelayedEvent;

    private bool executing;
    private float elapsedTime;

    public void Execute()
    {
        executing = true;
        elapsedTime = 0f;
    }

    private void Update()
    {
        if (executing && elapsedTime < DelaySeconds)
        {
            elapsedTime += Time.deltaTime;
            if (elapsedTime >= DelaySeconds)
            {
                DelayedEvent.Invoke();
            }
        }
    }
}
