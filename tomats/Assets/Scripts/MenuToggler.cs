﻿using UnityEngine;
using System.Collections;

public class MenuToggler : MonoBehaviour
{
    public GameObject Menu;

    public void Toggle()
    {
        Menu.SetActive(!Menu.activeSelf);
    }
}
