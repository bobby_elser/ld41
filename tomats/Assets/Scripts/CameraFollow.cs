﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    public GameObject Target;
    public float MaxLeadDistance = 5f;
    public float LeadMinSpeed = 2f;
    public float LeadMaxSpeed = 20f;
    [Range(0f,1f)]
    public float LeadChangeRate = 0.6f;

    private void Update()
    {
        if (Target != null)
        {
            Vector2 newTargetPosition = Target.transform.position;
            transform.position = new Vector3(newTargetPosition.x, newTargetPosition.y, transform.position.z);
        }
    }
}
