﻿using UnityEngine;
using System.Collections;

public abstract class FaderBase : MonoBehaviour
{
    [Range(0f, 1f)]
    public float minAlpha = 0f;
    [Range(0f, 1f)]
    public float maxAlpha = 1f;
    public float FullFadeSeconds = 5f;

    private float currentFadeRate = 0f;

    protected abstract Color toFade { get; set; }

    private void Update()
    {
        if (currentFadeRate > 0f && toFade.a < maxAlpha)
        {
            fadeBy(currentFadeRate * Time.deltaTime);
        }
        else if (currentFadeRate < 0f && toFade.a > minAlpha)
        {
            fadeBy(currentFadeRate * Time.deltaTime);
        }
        else
        {
            currentFadeRate = 0f;
        }
    }

    private void fadeBy(float fadeAmount)
    {
        Color newColor = toFade;
        newColor.a += fadeAmount;
        Mathf.Clamp(newColor.a, minAlpha, maxAlpha);
        toFade = newColor;
    }

    public void FadeIn()
    {
        currentFadeRate = 1f / FullFadeSeconds;
    }

    public void FadeOut()
    {
        currentFadeRate = -1f / FullFadeSeconds;
    }
}
