﻿using UnityEngine;

public class SpriteFader : FaderBase
{
    protected override Color toFade
    {
        get
        {
            return sprite.color;
        }
        set
        {
            sprite.color = value;
        }
    }

    private SpriteRenderer sprite;

    private void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
    }
}
