﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextFader : FaderBase
{
    protected override Color toFade
    {
        get
        {
            return text.color;
        }
        set
        {
            text.color = value;
        }
    }

    private Text text;

    private void Start()
    {
        text = GetComponent<Text>();
    }
}
