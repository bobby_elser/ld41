﻿using UnityEngine;

public class CanvasGroupFader : FaderBase
{
    private CanvasGroup canvasGroup;

    protected override Color toFade
    {
        get
        {
            return new Color(0,0,0,canvasGroup.alpha);
        }

        set
        {
            canvasGroup.alpha = value.a;
        }
    }

    private void Start()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }
}
