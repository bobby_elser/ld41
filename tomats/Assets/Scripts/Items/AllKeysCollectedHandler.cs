﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AllKeysCollectedHandler : MonoBehaviour
{
    public GameEvent AllKeysCollectedEvent;
    public List<GameEvent> KeyCollectionEvents;
    public KeyCollectionVariable KeyCollectionVariable;

    private bool allKeysAlreadyCollected = false;

    private void Start()
    {
        foreach(GameEvent keyCollectionEvent in KeyCollectionEvents)
        {
            keyCollectionEvent.OnRaise += OnKeyCollected;
        }
    }

    private void OnKeyCollected()
    {
        if (!allKeysAlreadyCollected && KeyCollectionVariable.Value.Count == KeyCollectionEvents.Count)
        {
            AllKeysCollectedEvent.Raise();
        }
    }
}
