﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class HeldItemDescription : MonoBehaviour
{
    public HoldableItemVariable HoldableItemVariable;

    private Text label;

    private void Start()
    {
        label = GetComponent<Text>();
    }

    public void UpdateDescription()
    {
        if (HoldableItemVariable.Value == null)
        {
            label.text = "Holding: nothing";
        }
        else
        {
            label.text = "Holding: " + HoldableItemVariable.Value.name;
        }
    }
}
