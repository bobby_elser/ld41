﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(HeldItem))]
public class KeyPickup : MonoBehaviour
{
    public KeyCollectionVariable PlayerKeyCollectionVariable;
    private  GameEvent OnKeyAdded;
    public SpiritKeyData KeyData;
    private HeldItem pickupItemHolder;

    void Start()
    {
        pickupItemHolder = GetComponent<HeldItem>();
        pickupItemHolder.OnDrop += this.OnPickup;
    }

    private void OnPickup(HeldItem holder)
    {
        if (!PlayerKeyCollectionVariable.Value.Contains(KeyData))
        {
            PlayerKeyCollectionVariable.Value.Add(KeyData);
            KeyData.KeyAddedEvent.Raise();
        }
    }
}
