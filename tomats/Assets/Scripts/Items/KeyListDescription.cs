﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class KeyListDescription : MonoBehaviour
{
    public KeyCollectionVariable PlayerKeyCollection;

    private Text label;

    private void Start()
    {
        label = GetComponent<Text>();
    }

    public void UpdateDescription()
    {
        string labelText = "";
        foreach (SpiritKeyData key in PlayerKeyCollection.Value)
        {
            labelText += key.name + "\n";
        }
        label.text = labelText;
    }
}
