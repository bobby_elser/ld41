﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

[RequireComponent(typeof(HeldItem))]
public class SinglePickup : Destructibler
{
    public UnityEvent OnPickup;
    private HeldItem pickupItemHolder;
    private List<PickupCondition> pickupConditions;

    private void Start()
    {
        pickupItemHolder = GetComponent<HeldItem>();
        pickupItemHolder.OnDrop += this.PickupHandler;

        PickupCondition[] conditions = GetComponents<PickupCondition>();
        pickupConditions = new List<PickupCondition>(conditions);
    }

    private void PickupHandler(HeldItem holder)
    {
        OnPickup.Invoke();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PickupHandler pickerUpper = collision.GetComponent<PickupHandler>();
        if (pickerUpper != null && pickupConditionsMet(pickerUpper))
        {
            pickerUpper.Pickup(pickupItemHolder.Item);
            // TODO: confirm pickerupper is holding item before dropping it?
            pickupItemHolder.Drop();
        }
    }

    private bool pickupConditionsMet(PickupHandler pickerUpper)
    {
        if (pickupConditions.Count > 0)
        {
            foreach (PickupCondition condition in pickupConditions)
            {
                if (condition.IsPickupAllowed(pickerUpper))
                {
                    return true;
                }
            }
            return false;
        }
        else
        {
            return true;
        }
    }
}
