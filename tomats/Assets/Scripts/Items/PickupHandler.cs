﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class PickupHandler : MonoBehaviour
{
    public HoldableItemVariable ItemVariable;
    public GameEvent OnPickup;

    public void Pickup(HoldableItem item)
    {
        ItemVariable.Value = item;
        if (OnPickup != null)
        {
            OnPickup.Raise();
        }
    }
}
