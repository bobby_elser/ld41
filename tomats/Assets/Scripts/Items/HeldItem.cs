﻿using UnityEngine;
using System.Collections;

public class HeldItem : MonoBehaviour
{
    public delegate void HeldItemDroppedHandler(HeldItem holder);
    public event HeldItemDroppedHandler OnDrop;

    public HoldableItem Item;

    public void Drop()
    {
        Item = null;
        if (OnDrop != null) OnDrop(this);
    }
}
