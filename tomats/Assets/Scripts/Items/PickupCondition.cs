﻿using UnityEngine;
using System.Collections;

public abstract class PickupCondition : MonoBehaviour
{
    public abstract bool IsPickupAllowed(PickupHandler pickerUpper);
}
