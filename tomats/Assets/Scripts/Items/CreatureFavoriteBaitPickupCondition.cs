﻿using UnityEngine;
using System.Collections;

public class CreatureFavoriteBaitPickupCondition : PickupCondition
{
    public HoldableItem Favorite;

    public override bool IsPickupAllowed(PickupHandler pickerUpper)
    {
        return (Favorite == null || pickerUpper.ItemVariable.Value == Favorite);
    }
}
