﻿using UnityEngine;

public class RaiseEventOnKeyDown: MonoBehaviour
{
    public GameEvent EventToRaise;
    public KeyCode KeyToRaiseFor;

    private void Update()
    {
        if (Input.GetKeyDown(KeyToRaiseFor))
        {
            EventToRaise.Raise();
        }
    }
}
