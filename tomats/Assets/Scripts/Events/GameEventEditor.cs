﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameEvent))]
public class GameEventEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GameEvent targetEvent = (GameEvent)target;
        if (GUILayout.Button("Raise Event"))
            targetEvent.Raise();
    }
}