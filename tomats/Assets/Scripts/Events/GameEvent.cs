﻿using UnityEngine;

[CreateAssetMenu(menuName = "LD41/Game Event")]
public class GameEvent : ScriptableObject
{
    public delegate void GameEventHandler();
    public event GameEventHandler OnRaise;

    public void Raise()
    {
        if (OnRaise != null)
            OnRaise();
    }

    public void RegisterListener(GameEventListener listener)
    {
        OnRaise += listener.OnEventRaised;
    }

    public void UnregisterListener(GameEventListener listener)
    {
        OnRaise -= listener.OnEventRaised;
    }
}