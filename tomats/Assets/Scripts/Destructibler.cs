﻿using UnityEngine;
using System.Collections;

public class Destructibler : MonoBehaviour
{
    public void DestroySelf()
    {
        Destroy(this.gameObject);
    }
}
