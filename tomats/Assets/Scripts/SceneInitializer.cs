﻿using UnityEngine;
using System.Collections;

public class SceneInitializer : MonoBehaviour
{
    public MapData Map;
    public MapVariable CurrentMapVariable;
    public GameObject WallPrefab;
    public SinglePickup PickupPrefab;
    public float StarterBaitPlacementRadius;
    public SpawnArea SpawnAreaPrefab;
    public GameObject CreaturePrefab;
    public GameObject SpiritPrefab;

    private void Start()
    {
        CurrentMapVariable.Value = Map;
        setMapBounds();
        placeStarterBaitSpawns();
        placeCreatureSpawns();
        placeSpiritSpawns();
        Destroy(this);
    }
    
    private void setMapBounds()
    {
        GameObject southWall = Instantiate(WallPrefab, new Vector2(0, -Map.MapHeight/2), Quaternion.identity);
        GameObject northWall = Instantiate(WallPrefab, new Vector2(0, Map.MapHeight / 2), Quaternion.Euler(0, 0, 180));
        GameObject eastWall = Instantiate(WallPrefab, new Vector2(Map.MapWidth / 2, 0), Quaternion.Euler(0, 0, 90));
        GameObject westWall = Instantiate(WallPrefab, new Vector2(-Map.MapWidth / 2, 0), Quaternion.Euler(0, 0, -90));

        southWall.transform.localScale = new Vector2(Map.MapWidth, 1f);
        northWall.transform.localScale = new Vector2(Map.MapWidth, 1f);
        eastWall.transform.localScale = new Vector2(Map.MapHeight, 1f);
        westWall.transform.localScale = new Vector2(Map.MapHeight, 1f);

        southWall.name = "South Wall";
        northWall.name = "North Wall";
        eastWall.name = "East Wall";
        westWall.name = "West Wall";
    }

    private void placeStarterBaitSpawns()
    {
        int placedSpawnCount = 0;
        float placementInterval = 360f / Map.StarterBaits.Count;

        foreach(BaitData baitData in Map.StarterBaits)
        {
            SinglePickup baitPickupTemplate = Instantiate(PickupPrefab);
            baitPickupTemplate.GetComponent<SpriteRenderer>().color = baitData.PickupColor;
            baitPickupTemplate.GetComponent<HeldItem>().Item = baitData;
            baitPickupTemplate.gameObject.SetActive(false);
            baitPickupTemplate.name = baitData.name + " pickup template";

            SpawnArea pickupSpawn = Instantiate(SpawnAreaPrefab);
            pickupSpawn.PrefabToSpawn = baitPickupTemplate.gameObject;
            pickupSpawn.TargetPopulationSize = 1;
            pickupSpawn.SpawnTime = 2f;
            pickupSpawn.Radius = 0.1f;
            pickupSpawn.transform.position = Quaternion.Euler(0, 0, placedSpawnCount * placementInterval) * (Vector2.right * StarterBaitPlacementRadius);

            placedSpawnCount++;
        }
    }

    private void placeCreatureSpawns()
    {
        foreach(CreatureSpawnAreaData spawnData in Map.CreatureSpawnAreas)
        {
            GameObject creatureTemplate = Instantiate(CreaturePrefab);
            creatureTemplate.GetComponent<HeldItem>().Item = spawnData.CreatureToSpawn.BaitDropped;
            foreach(BaitData favoredBait in spawnData.CreatureToSpawn.FavoredBait)
            {
                creatureTemplate.AddComponent<CreatureFavoriteBaitPickupCondition>().Favorite = favoredBait;
            }
            creatureTemplate.transform.localScale *= spawnData.CreatureToSpawn.ShadowSize;
            creatureTemplate.SetActive(false);
            creatureTemplate.name = spawnData.CreatureToSpawn.name + " template";

            SpawnArea creatureSpawn = Instantiate(SpawnAreaPrefab);
            creatureSpawn.PrefabToSpawn = creatureTemplate.gameObject;
            creatureSpawn.TargetPopulationSize = spawnData.TargetPopulation;
            creatureSpawn.SpawnTime = spawnData.SpawnTime;
            creatureSpawn.Radius = spawnData.Radius;
            creatureSpawn.transform.position = spawnData.Position;
        }
    }

    private void placeSpiritSpawns()
    {
        //foreach (CreatureSpawnAreaData spawnData in Map.SpiritCreatureSpawnAreas)
        foreach (SpiritData spiritData in Map.Spirits)
        {
            CreatureSpawnAreaData spawnData = spiritData.SpawnArea;
            GameObject spiritTemplate = Instantiate(SpiritPrefab);
            spiritTemplate.GetComponent<KeyPickup>().KeyData = spiritData.Key;
            foreach (BaitData favoredBait in spawnData.CreatureToSpawn.FavoredBait)
            {
                spiritTemplate.AddComponent<CreatureFavoriteBaitPickupCondition>().Favorite = favoredBait;
            }
            spiritTemplate.transform.localScale *= spawnData.CreatureToSpawn.ShadowSize;
            spiritTemplate.SetActive(false);
            spiritTemplate.name = spawnData.CreatureToSpawn.name + " template";

            SpawnArea creatureSpawn = Instantiate(SpawnAreaPrefab);
            creatureSpawn.PrefabToSpawn = spiritTemplate.gameObject;
            creatureSpawn.TargetPopulationSize = spawnData.TargetPopulation;
            creatureSpawn.SpawnTime = spawnData.SpawnTime;
            creatureSpawn.Radius = spawnData.Radius;
            creatureSpawn.transform.position = spawnData.Position;
        }
    }
}
