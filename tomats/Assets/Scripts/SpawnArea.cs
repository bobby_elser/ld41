﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SpawnArea : MonoBehaviour
{
    public GameObject PrefabToSpawn;
    public int TargetPopulationSize = 1;
    public float SpawnTime = 3f;
    public float Radius = 2f;

    private List<GameObject> spawnedInstances;
    private DateTime nextSpawn;
    private bool aboutToSpawn = false;

    private void OnDrawGizmos()
    {
        UnityEditor.Handles.color = Color.cyan;
        UnityEditor.Handles.DrawWireDisc(transform.position, Vector3.forward, Radius);
    }

    private void Start()
    {
        spawnedInstances = new List<GameObject>();
        while (spawnedInstances.Count < TargetPopulationSize)
        {
            spawnOne();
        }
    }

    private void Update()
    {
        evaluateSpawnCondition();
    }

    private void evaluateSpawnCondition()
    {
        for (int i = spawnedInstances.Count - 1; i >= 0; i--)
        {
            if (spawnedInstances[i] == null)
            {
                //Debug.Log("Instance destroyed, removing from spawn tracker [" + gameObject.name + "]");
                spawnedInstances.RemoveAt(i);
            }
        }
        if (nextSpawn <= DateTime.Now && spawnedInstances.Count < TargetPopulationSize)
        {
            if (!aboutToSpawn)
            {
                //Debug.Log("Spawn tracker [" + gameObject.name + "] is below population target, starting spawn timer");
                aboutToSpawn = true;
                nextSpawn = DateTime.Now.AddSeconds(SpawnTime);
            }
            else
            {
                spawnOne();
                if (spawnedInstances.Count < TargetPopulationSize)
                {
                    //Debug.Log("Spawn tracker [" + gameObject.name + "] is still below population target, restarting spawn timer");
                    nextSpawn = DateTime.Now.AddSeconds(SpawnTime);
                }
                else
                {
                    //Debug.Log("Spawn tracker [" + gameObject.name + "] is at population target");
                    aboutToSpawn = false;
                }
            }
        }
    }

    private void spawnOne()
    {
        Vector3 position = transform.position + UnityEngine.Random.rotation * Vector2.up * UnityEngine.Random.Range(0f, Radius);
        position.z = PrefabToSpawn.transform.position.z;
        GameObject newInstance = Instantiate(PrefabToSpawn, position, Quaternion.identity);
        newInstance.SetActive(true);
        spawnedInstances.Add(newInstance);
        //Debug.Log("[" + gameObject.name + "] spawned one instance at " + position.x + "," + position.y);
    }
}
