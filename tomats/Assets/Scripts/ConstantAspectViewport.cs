﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class ConstantAspectViewport : MonoBehaviour
{
    public float Aspect = 1f;
    public float MaxWidthNorm = 0.3f;
    public float MaxHeightNorm = 0.3f;
    private Camera cam;
    private float previousScreenAspect = 0f;

    private void Start()
    {
        cam = GetComponent<Camera>();
    }

    private void Update()
    {
        float newAspect = (float) Screen.width / (float) Screen.height;
        if (newAspect != previousScreenAspect)
        {
            float camWidth = MaxWidthNorm * Screen.width;
            float camHeight = MaxHeightNorm * Screen.height;
            float aspectAtMax = camWidth / camHeight;

            if (aspectAtMax > Aspect)
            {
                camWidth = camHeight * Aspect;
                Debug.Log("aspect at max " + aspectAtMax + " greater than target, adjust camWidth to " + camWidth + ", keep camHeight " + camHeight);
            }
            else if (aspectAtMax < Aspect)
            {
                camHeight = camWidth / Aspect;
                Debug.Log("aspect at max " + aspectAtMax + " less than target, adjust camHeight to " + camHeight + ", keep camWidth " + camWidth);
            }
            else
            {
                Debug.Log("aspect at max " + aspectAtMax + " equal to target, keep camHeight " + camHeight + " and camWidth " + camWidth);
            }

            cam.pixelRect = new Rect(15, Screen.height - (camHeight + 15), camWidth, camHeight);

            previousScreenAspect = newAspect;
        }
    }
}
